# Save It  

![image info](./repo-pic.jpg)

I watch a lot of Youtube videos and would like to save some for when my home internet connection drops.
I modified the code found in the FreeCodeCamp guide (https://www.freecodecamp.org/news/python-program-to-download-youtube-videos/) to download a list of videos stored in a text file.  

Create a file called links.txt and place youtube urls in it for download.  

Run the script  
In the same directory that the links.txt file is in, run ./save-it.py
