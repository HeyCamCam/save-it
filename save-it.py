#!/usr/bin/env python3

import os
from pytube import YouTube, Playlist

# read youtube links into memory
txt_file = open("links.txt", "r")
links = txt_file.readlines()

def dl_video(link):
    youtubeObject = YouTube(link)
    youtubeObject = youtubeObject.streams.get_highest_resolution()
    try:
        print(f"Downloading {YouTube(link).title}", end='')
        youtubeObject.download()
    except:
        print("Error")
    print("Download complete")

# loop through links and download them
for link in links:
    if "playlist" in link:
        theplaylist = Playlist(link)
        for link in theplaylist:
            dl_video(link)
    else:
        dl_video(link)
